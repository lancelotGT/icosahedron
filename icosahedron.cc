// Draw an Icosahedron
// ECE4893/8893 Project 4
// Ning Wang

#include <iostream>
#include <math.h>
#include <GL/glut.h>
#include <GL/glext.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <vector>
#include <algorithm>

using namespace std;

#define NFACE 20
#define NVERTEX 12

#define X .525731112119133606
#define Z .850650808352039932

// These are the 12 vertices for the icosahedron
static GLfloat vdata[NVERTEX][3] = {
    {-X, 0.0, Z}, {X, 0.0, Z}, {-X, 0.0, -Z}, {X, 0.0, -Z},
    {0.0, Z, X}, {0.0, Z, -X}, {0.0, -Z, X}, {0.0, -Z, -X},
    {Z, X, 0.0}, {-Z, X, 0.0}, {Z, -X, 0.0}, {-Z, -X, 0.0}
};

// These are the 20 faces.  Each of the three entries for each
// vertex gives the 3 vertices that make the face.
static GLint tindices[NFACE][3] = {
    {0,4,1}, {0,9,4}, {9,5,4}, {4,5,8}, {4,8,1},
    {8,10,1}, {8,3,10}, {5,3,8}, {5,2,3}, {2,7,3},
    {7,10,3}, {7,6,10}, {7,11,6}, {11,0,6}, {0,1,6},
    {6,1,10}, {9,0,11}, {9,11,2}, {9,2,5}, {7,2,11} };

typedef struct vertex_t
{
    GLfloat x;
    GLfloat y;
    GLfloat z;
} vertex;

typedef struct face_t
{
    vertex v0;
    vertex v1;
    vertex v2;
} face;

static vector<face>* faces;

int testNumber; // Global variable indicating which test number is desired
static int depth = 1;
static int updateRate = 50;

void drawIcosahedron()
{
    for (int i = 0; i < NFACE; i++) {
        GLfloat* points[] = {
            vdata[tindices[i][0]],
            vdata[tindices[i][1]],
            vdata[tindices[i][2]]
        };
        
        // color the face
        glColor3f(1, 1.0 / NFACE * i, 1.0 / NFACE * i);
        glBegin(GL_TRIANGLES);
        glVertex3fv(points[0]);
        glVertex3fv(points[1]);
        glVertex3fv(points[2]);
        glEnd();
    }
    
    glColor3f(1.0, 1.0, 1.0);
    for (int i = 0; i < NFACE; i++) {
        
        GLfloat* points[] = {
            vdata[tindices[i][0]],
            vdata[tindices[i][1]],
            vdata[tindices[i][2]]
        };

        // draw lines on each face
        glBegin(GL_LINES);
        glVertex3fv(points[0]);
        glVertex3fv(points[1]);
        
        glVertex3fv(points[0]);
        glVertex3fv(points[2]);
        glVertex3fv(points[1]);
        glVertex3fv(points[2]);
        glEnd();
    }
}


vertex getMidPoint(vertex A, vertex B)
{
    vertex vtx;
    vtx.x = (A.x + B.x) / 2;
    vtx.y = (A.y + B.y) / 2;
    vtx.z = (A.z + B.z) / 2;
    
    GLfloat divisor = sqrt(vtx.x * vtx.x + vtx.y * vtx.y + vtx.z * vtx.z);
    vtx.x = vtx.x / divisor;
    vtx.y = vtx.y / divisor;
    vtx.z = vtx.z / divisor;
    return vtx;
}


void subdivideTriangle()
{
    faces = new vector<face>();

    for (int i = 0; i < NFACE; i++)
    {
        face tmp;
        tmp.v0.x = vdata[tindices[i][0]][0];
        tmp.v0.y = vdata[tindices[i][0]][1];
        tmp.v0.z = vdata[tindices[i][0]][2];
        
        tmp.v1.x = vdata[tindices[i][1]][0];
        tmp.v1.y = vdata[tindices[i][1]][1];
        tmp.v1.z = vdata[tindices[i][1]][2];
        
        tmp.v2.x = vdata[tindices[i][2]][0];
        tmp.v2.y = vdata[tindices[i][2]][1];
        tmp.v2.z = vdata[tindices[i][2]][2];

        faces->push_back(tmp);
    }
    
    for (int i = 0; i < depth; i++) {
        vector<face>* new_faces = new vector<face>();
        while (!faces->empty())
        {
            face this_one = faces->back();
            faces->pop_back();
            
            vertex a = getMidPoint(this_one.v0, this_one.v1);
            vertex b = getMidPoint(this_one.v1, this_one.v2);
            vertex c = getMidPoint(this_one.v2, this_one.v0);
            
            // add new faces
            face face0;
            face0.v0 = this_one.v0;
            face0.v1 = a;
            face0.v2 = c;
            
            face face1;
            face1.v0 = this_one.v1;
            face1.v1 = a;
            face1.v2 = b;
            
            face face2;
            face2.v0 = this_one.v2;
            face2.v1 = c;
            face2.v2 = b;
            
            face face3;
            face3.v0 = a;
            face3.v1 = b;
            face3.v2 = c;
            
            new_faces->push_back(face0);
            new_faces->push_back(face1);
            new_faces->push_back(face2);
            new_faces->push_back(face3);
        }
        faces = new_faces;
    }
    random_shuffle(faces->begin(), faces->end());
}

void drawMoreIcosahedron()
{
    int i = 0;
    for (vector<face>::iterator it = faces->begin() ; it != faces->end(); ++it)
    {
        i++;
        int size = (int) faces->size();
        face this_one = *it;
        
        // color the face
        glColor3f(0.8, 1.0 / size * i, 1.0 / size * i);
        glBegin(GL_TRIANGLES);
        glVertex3f(this_one.v0.x, this_one.v0.y, this_one.v0.z);
        glVertex3f(this_one.v1.x, this_one.v1.y, this_one.v1.z);
        glVertex3f(this_one.v2.x, this_one.v2.y, this_one.v2.z);
        glEnd();
    }
    i = 0;
    
    glColor3f(1.0, 1.0, 1.0);
    for (vector<face>::iterator it = faces->begin() ; it != faces->end(); ++it)
    {
        i++;
        face this_one = *it;
        
        // color the face
        glBegin(GL_LINES);
        glVertex3f(this_one.v0.x, this_one.v0.y, this_one.v0.z);
        glVertex3f(this_one.v1.x, this_one.v1.y, this_one.v1.z);
        
        glVertex3f(this_one.v0.x, this_one.v0.y, this_one.v0.z);
        glVertex3f(this_one.v2.x, this_one.v2.y, this_one.v2.z);
        
        glVertex3f(this_one.v1.x, this_one.v1.y, this_one.v1.z);
        glVertex3f(this_one.v2.x, this_one.v2.y, this_one.v2.z);
        glEnd();
    }
}

// Test cases.  Fill in your code for each test case
void Test1()
{
    static int pass;
    
    cout << "Displaying pass " << ++pass << endl;
    // clear all
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Clear the matrix
    glLoadIdentity();
    // Set the viewing transformation
    gluLookAt(0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0);
    
    drawIcosahedron();
    
    // Flush buffer
    glutSwapBuffers(); // If double buffering
}

void Test2()
{
    static int pass;
    
    cout << "Displaying pass " << ++pass << endl;
    // clear all
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Clear the matrix
    glLoadIdentity();
    // Set the viewing transformation
    gluLookAt(0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0);
    
    // Try rotating
    static GLfloat rotX = 0.0;
    static GLfloat rotY = 0.0;
    glRotatef(rotX, 1.0, 0.0, 0.0);
    glRotatef(rotY, 0.0, 1.0, 0.0);
    rotX += 1.0;
    rotY -= 1.0;
    
    drawIcosahedron();
    
    // Flush buffer
    glutSwapBuffers(); // If double buffering
}

void Test3()
{
    static int pass;
    
    cout << "Displaying pass " << ++pass << endl;
    // clear all
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Clear the matrix
    glLoadIdentity();
    // Set the viewing transformation
    gluLookAt(0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0);
    
    drawMoreIcosahedron();
    
    // Flush buffer
    glutSwapBuffers(); // If double buffering
}

void Test4()
{
    static int pass;
    
    cout << "Displaying pass " << ++pass << endl;
    // clear all
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    // Clear the matrix
    glLoadIdentity();
    // Set the viewing transformation
    gluLookAt(0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0);
    
    // Try rotating
    static GLfloat rotX = 0.0;
    static GLfloat rotY = 0.0;
    glRotatef(rotX, 1.0, 0.0, 0.0);
    glRotatef(rotY, 0.0, 1.0, 0.0);
    rotX += 1.0;
    rotY -= 1.0;
    
    drawMoreIcosahedron();
    
    // Flush buffer
    glutSwapBuffers(); // If double buffering

}

void Test5(int d)
{
    depth = d;
    subdivideTriangle();
    glutDisplayFunc(Test3);
}

void Test6(int d)
{
    depth = d;
    subdivideTriangle();
    glutDisplayFunc(Test4);
}

void init()
{
    //select clearing (background) color
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    glLineWidth(2.0);
}

void reshape(int w, int h)
{
    glViewport(0,0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
//    glOrtho(0.0, (GLdouble)w, (GLdouble)0.0, h, (GLdouble)-w, (GLdouble)w);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void timer(int)
{ // mark the current window as needing redisplay
    glutPostRedisplay();
    glutTimerFunc(1000.0 / updateRate, timer, 0); // will call timer again and again
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        std::cout << "Usage: icosahedron testnumber" << endl;
        exit(1);
    }
    // Set the global test number
    testNumber = atoi(argv[1]);
    
    // Initialize glut  and create your window here
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Icosahedron");
    init();
    
    // Set your glut callbacks here
    switch (testNumber) {
        case 1:
            glutDisplayFunc(Test1);
            break;
        case 2:
            glutDisplayFunc(Test2);
            break;
        case 3:
            subdivideTriangle();
            glutDisplayFunc(Test3);
            break;
        case 4:
            subdivideTriangle();
            glutDisplayFunc(Test4);
            break;
        case 5:
            if (argc == 3)
                Test5(atoi(argv[2]));
            else
            {
                subdivideTriangle();
                glutDisplayFunc(Test3);
            }
            break;
        case 6:
            if (argc == 3)
                Test6(atoi(argv[2]));
            else
            {
                subdivideTriangle();
                glutDisplayFunc(Test4);
            }
            break;
        default:
            break;
    }
    
    glutReshapeFunc(reshape);
    glutTimerFunc(1000.0 / updateRate, timer, 0);
    
    // Enter the glut main loop here
    glutMainLoop();
    return 0;
}

